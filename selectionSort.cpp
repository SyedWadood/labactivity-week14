#include "selectionSort.h"
#include <vector>
void SelectionSort::sort(std::vector<int> &list)
{
  int aux,x;
  for(int j=0;j< list.size()-1; j++)
    {
      x=j;
      for(int i=j+1;i<list.size();i++)
	{
	  if(list[i]<list[x])
	     x=i;
	}
      if(x!=j)
	{
	  aux=list[j];
	  list[j]=list[x];
	  list[x] = aux;
	}
    }
}
